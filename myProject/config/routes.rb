Rails.application.routes.draw do
  resources :users
  get 'tweet_page/show' 
  root 'tweet_page#show'
  post '/users' =>'users#create'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
